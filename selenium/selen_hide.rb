require "selenium/webdriver"
#require "capybara-selenium"

driver_name = :selenium
browser_name = :chrome
options = {}

Capybara.register_driver driver_name do |app|
  driver_options = {browser: browser_name}.merge(options)
  Capybara::Selenium::Driver.new(app, driver_options)
end

Capybara.javascript_driver = :headless_chrome