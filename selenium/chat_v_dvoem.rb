require "selenium-webdriver"

browser = Selenium::WebDriver.for :chrome
browser.get "http://xn--80aeahg6bm2a7b.xn--p1ai/"

el_text = 'textarea.input-block-level.send-message-line'
el_send_button = '//*[@id="sendmessage"]/button'


wait = Selenium::WebDriver::Wait.new(:timeout => 25)

while true
  sleep 5
  element = browser.find_element(:css, el_text)
  element.send_keys "Привет"
  browser.find_element(:xpath, el_send_button).click

  posts = browser.find_elements(:css, 'div#wrap-chat table > tbody > tr > td.msg') rescue nil
  posts.each { |pp| p pp.text  } unless posts.nil?

  btn_save= browser.find_element(:css, 'a.btn.hidesave') rescue nil
  break if !btn_save.nil? && btn_save.displayed?
end

wait.until do
  browser.find_element(:css, 'a.btn.hidesave')
end

#browser.quit
exit
