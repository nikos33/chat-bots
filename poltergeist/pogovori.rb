require 'cgi'
require 'timeout'
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'parallel'
require 'rufus-scheduler'
require_relative 'bot_brain'

class PSMChatBot
  include Capybara::DSL
  attr_accessor :num, :chatid, :opp_msgs, :opp_session, :show_log
  USE_TOR = false

  def initialize(indx)
    Capybara.register_driver :polt do |app|
      phantomjs_options = ['--ignore-ssl-errors=yes', '--ssl-protocol=any', '--load-images=no']
      #phantomjs_options.push('--proxy=localhost:9050', '--proxy-type=socks5') #if #Rails.env.production?
      Capybara::Poltergeist::Driver.new(
        app,
        js_errors: true, # break on js error
        timeout: 180, # maximum time in second for the server to produce a response
        debug: false, # more verbose log
        window_size: [1280, 800], # not responsive, used to simulate scroll when needed
        inspector: false, # use debug breakpoint and chrome inspector,
        phantomjs_options: phantomjs_options
      )
    end
    Capybara.default_driver = :polt

    #Capybara.driver.header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36')
    #Capybara.current_session.driver.header('Accept-Language', 'en-US,en;q=0.8')
    #Capybara.current_session.driver.header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
    #Capybara.current_session.driver.header('Accept-Encoding', 'gzip, deflate, sdch')
    #Capybara.current_session.driver.header('Connection', 'keep-alive')
    @num=indx
    @chatid = DateTime.now.strftime("%H%M%S").to_i
    @opp_msgs=[]

  end

  def log(text)
    p "[#{@num}] #{text}"
  end

  def say(msg)

    if page.has_css?('button.btn.btn-large') && page.has_css?('textarea#chat-form-message')
      find('textarea#chat-form-message').set(msg)
      log("---------- "+msg)
      find('button.btn.btn-large').click #if page.has_css?('button.btn.btn-large')
    end
  end

  def check_ip
    visit "http://wtfismyip.com/"
    log page.text
  end

  def talk
    
    #check_ip

    visit "http://pogovorisomnoi.ru/"
    bot = BotAI.new(@num,'fun', 'by')

    start_session()

    last_msg_num=0
    last_text=""
    first_mes=true
    no_answered_counter=0

    @show_log = true
    #---------talk type
    use_x_x_sessions = false
    while true

      if first_mes
        say bot.say('greeting')
        #last_text = say("меня зовут #{bot.say('name')}")
        first_mes = false
        no_answered_counter=0
        #@opp_msgs=[]
      end
      sleep(5+rand(1))

      last_text = say_from_file(bot, last_text)

      if false # вставить строки текста из файла с разныи песнями
        say((line=bot.seq_talk).size<2 ? bot.seq_talk : line) if @show_log
      end

      if use_x_x_sessions
        say(@opp_session.opp_msgs.shift) if  @opp_session && @opp_session.opp_msgs.size>0
      end

      no_answered_counter+=1

      page.all('div#conversation-log div.message:not(.mine):not(.system)').each_with_index do |div,ind|

        if  ind>=last_msg_num
          msg = div.text
          log("--- #{msg}")
          @opp_msgs << msg

          #bot.save_message(@chatid,msg)
          #answ=msg.split(' ').shuffle.join(' ') #bot.seq_talk()
          #say(answ) if last_text.empty?

          last_msg_num+=1
          no_answered_counter=0
        end
      end

      if page.has_css?('div#conversation-log button.btn')
        #bot.init_talk()
        first_mes = true
        #last_text = ""
        #first('div#conversation-log button.btn').click
        click_button('новая беседа')
        log "another session finished"  if @show_log
      end

      if  no_answered_counter>4
        #bot.init_talk()
        first_mes = true
        #last_text = ""
        click_finish
        click_button('новая беседа') if page.has_css?('div#conversation-log button.btn')
        log "no_answered_counter >>session finished"  if @show_log
      end
    end
    #log "finished!!!"
  end

  def say_from_file(bot,last_text)
    line = bot.phrase_from_file #phrase_from_file

    if   line && line!=last_text && !line.empty?
      if  line.start_with?("change_seq")
        id = line[-1].to_i
        bot.init_seq_text(id)
      else
        say(line)
        #@opp_msgs<<line

      end
    end
    line
  end

  def start_session
    sleep(1)
    if page.has_css?('button#start-chat')
      log "start chatting"
      find('button#start-chat').click
    end
  end

  def click_finish()
    find('button#stop-conversation').click if page.has_css?('button#stop-conversation')
    sleep 1
    #page.accept_confirm { click_button "OK" } #accept_alert
    #page.driver.accept_js_confirms!
    page.evaluate_script('window.confirm = function() { return true; }')

  end
end

def run_sheduler
  scheduler = Rufus::Scheduler.new
  period=4
  scheduler.interval "#{period}s" do
    p "start rufus job"
    PSMChatBot.new(1).talk
  end
  scheduler.join
end
#run_sheduler


def run_parallel
  p "run_parallel"
  number=2
  #PSMChatBot.new(1).check_ip if count ==100
  #PSMChatBot.new(1).talk if count ==1

  Parallel.each(1..number, :in_processes => number) do |i|
    PSMChatBot.new(i).talk
    #sleep 5+rand(3)
  end if number >1
end
#run_parallel

def test
  bot = BotAI.new(1,'fun', 'by')
  line = bot.phrase_from_file #phrase_from_file
  last_text=""
  if   line && line!=last_text && !line.empty?
    if  line.start_with?("change_seq")
      id = line[-1].to_i
      bot.init_seq_text(id)
      last_text = line
      15.times { |ii| p bot.seq_talk  }
    else
      #say(line)
      last_text = line
    end
  end
end

def chat_x_x

  session1 = PSMChatBot.new(1)
  session2 = PSMChatBot.new(2)

  session1.opp_session = session2
  session2.opp_session = session1

  sessions=[session1,session2]

  Parallel.each(sessions, :in_threads => sessions.size) do |sess|
    sess.talk
  end
end

PSMChatBot.new(1).talk  
#run_parallel 
#№chat_x_x 

