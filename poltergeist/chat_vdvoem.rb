require 'cgi'
require 'timeout'
require 'capybara'
require 'capybara/dsl'
require 'capybara/poltergeist'
require 'parallel'
require 'rufus-scheduler'
require_relative 'bot_brain'

class Ch2mChatbot
  include Capybara::DSL
  attr_accessor :num, :chatid

  def initialize(indx)
    Capybara.register_driver :polt do |app|
      phantomjs_options = ['--ignore-ssl-errors=yes', '--ssl-protocol=any', '--load-images=no']
      phantomjs_options.push('--proxy=localhost:9050', '--proxy-type=socks5') #if Rails.env.production?
      Capybara::Poltergeist::Driver.new(
        app,
        js_errors: false, # break on js error
        timeout: 180, # maximum time in second for the server to produce a response
        debug: false, # more verbose log
        window_size: [1280, 800], # not responsive, used to simulate scroll when needed
        inspector: false, # use debug breakpoint and chrome inspector,
        phantomjs_options: phantomjs_options
      )
    end
    Capybara.default_driver = :polt

    #Capybara.driver.header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36')
    #Capybara.current_session.driver.header('Accept-Language', 'en-US,en;q=0.8')
    #Capybara.current_session.driver.header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
    #Capybara.current_session.driver.header('Accept-Encoding', 'gzip, deflate, sdch')
    #Capybara.current_session.driver.header('Connection', 'keep-alive')
    @num=indx
    @chatid = DateTime.now.strftime("%H%M%S").to_i
  end

  def log(text)
    p "[#{@num}]>> #{text}"
  end

  def say(msg)
    find('textarea.input-block-level.send-message-line').set(msg)
    sleep 1
    log("-----"+msg)
    find('button.send-message-line').click
  end

  def check_ip
    visit "http://wtfismyip.com/"
    puts page.text
  end

  def talk
    
    visit "http://xn--80aeahg6bm2a7b.xn--p1ai/"
    #bot = BotAI.new('by', 'by')
    bot = BotAI.new(@num,'fun', 'fun')


    last_msg=0
    last_text=""
    first_mes=true
    no_answered_counter=0

    while true
      #p "ping"

      if first_mes
        sleep(2+rand(1))
        #p "greetings"
        say bot.first_m
        first_mes = false
        no_answered_counter=0
      end
      sleep(6+rand(1))

      if line = bot.phrase_from_file
        break if line=='fin'
        if line!=last_text && !line.empty?
          line=parse_and_say(line,bot)
          last_text = line
        end
      end

      no_answered_counter+=1

      page.all('table.table-striped tr').each_with_index do |tr,ind|
        photo=tr.all("td:nth-child(1)  a  img").first
        next if photo && photo[:title] == 'Вы'

        if  ind>=last_msg
          break if tr.first("div.msg").nil?

          msg = tr.all("div.msg").first.text
          next if  msg.empty? || msg.start_with?("Ваш собеседник прервал")

          log(msg)  #if !msg.start_with?("Ваш собеседник прервал")
          bot.save_message(@chatid,msg)
          answ = bot.parse_message(msg)
          #answ=msg.split(' ').shuffle.join(' ')

          say(answ) if last_text.empty?
          last_msg+=1
          no_answered_counter=0
        end

      end
      log "#{'*'*no_answered_counter}"

      if page.has_css?('a.btn.hidesave')
        bot.reset_talk()
        first_mes = true
        last_text = ""
        log "another session finished"
        #first('div#conversation-log button.btn').click
        all('a.btn.hidesave')[0].click
      end

      if  no_answered_counter>4
        bot.reset_talk()
        first_mes = true
        last_text = ""
        sleep(1)
        find('button#reconnect').click if page.has_css?('button#reconnect')
        log "no_answered_counter >>session finished"
      end
    end
    #log "finished!!!"
  end


  def parse_and_say(mes,bot)
    if mes=='next'
      mes  = bot.parse_message(mes)
    end
    say mes
    mes
  end
end

#
Ch2mChatbot.new(1).talk

def run_sheduler
  scheduler = Rufus::Scheduler.new
  period=4
  scheduler.interval "#{period}s" do
    p "start rufus job"
    Ch2mChatbot.new(1).talk
  end
  scheduler.join
end
#run_sheduler

def run_parallel
  number=2

  Parallel.each(1..number, :in_processes => number) do |i|
    Ch2mChatbot.new(i).talk
    #sleep 5+rand(3)
  end if number >1
end
#run_parallel
