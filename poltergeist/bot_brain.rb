require 'sequel'
require 'nokogiri'

class BotAI
  DB = Sequel.connect(:adapter => 'mysql2',:host => 'localhost',:database => 'fbot',:user => 'root')
  DBCHAT = Sequel.connect(:adapter => 'mysql2',:host => 'localhost',:database => 'chatbot',:user => 'root')

  attr_accessor :bot_id, :frec, :texts, :greetings

  def initialize(bot_id,greets, talks)
    @bot_id = bot_id
    @frec = Array.new(90,0)
    @greetings = File.readlines("data/greet/greetings_#{greets}.chat")
    @texts = File.readlines("data/talks/talks_#{talks}.chat")
    init_seq_text(@bot_id)
  end

  def init_seq_text(text_id, line_num=0)
    @seq_line_num=0
    @seq_texts = File.readlines("data/seq/seq#{text_id}.chat")
  end

  def save_message(chatid,msg)
    DBCHAT[:posts].insert({chat_id: chatid, text: msg, created: DateTime.now})
  end

  def say(msg_type)
    case msg_type
    when 'greeting'
      @greetings.sample.strip
    when 'name'
      names = File.readlines("data/names.chat")
      names.sample.strip
    when ''
    else
    end
  end

  def parse_message(msg)
    mm = msg.downcase.strip
    if  mm.start_with? 'привет'
      first_m
    elsif mm.start_with? 'cк лет'
      age_m
    else
      seq_talk
    end

  end

  def any
    #File.write("data_2.chat",rr.join("\n"))
    #if (txt=text_from_my_file) then return txt end

    loop do
      max =@frec.max
      max=1 if max==0
      r1 = rand(@texts.size)

      if @frec[r1]<max
        @frec[r1]+=1
        return @texts[r1].strip
      end
    end
  end

  def seq_talk()
    @seq_line_num+=1
    @seq_line_num=0 if @seq_line_num>@seq_texts.size-1
    
    txt =@seq_texts[@seq_line_num-1]
    txt.nil? ? "" : txt.strip
  end

  def  phrase_from_file()
    fname = "data/my#{@bot_id}.chat"
    lines = File.readlines(fname).first rescue ""
  end

  def get_line(filename, lineno)
    File.open(filename,'r') do |f|
      f.gets until f.lineno == lineno - 1
      f.gets
    end
  end
end

def test
  bb=BotAI.new
  mes='привет'
  #p bb.parse_message(mes)
  #10.times {p bb.any}
  #p bb.text_from_my_file
  p bb.say('name')

end
