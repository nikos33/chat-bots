require 'faye/websocket'
require 'eventmachine'

def load_headers
  File.readlines("psm-ws-headers.txt").map { |ll| ll.strip.split(':',2)  }.to_h
end


EM.run {
  ws = Faye::WebSocket::Client.new('ws://pogovorisomnoi.ru:8008/chat',[],
    :headers=> load_headers
    )

  ws.on :open do |event|
    p [:open]
    ws.send('Hello, world!')
  end

  ws.on :message do |event|
    p [:message, event.data]
  end

  ws.on :close do |event|
    p [:close, event.code, event.reason]
    ws = nil
  end

  loop do
    tt = STDIN.gets.strip
    p "sent: #{tt}"
    ws.send tt
  end
}
