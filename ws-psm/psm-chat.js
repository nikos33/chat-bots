var app = app || {};

app.models = {};

/* --- Models --- */
app.models.ChatState = Backbone.Model.extend({
    localStorage: new Backbone.LocalStorage('ChatState'),
    defaults: {
        state: 'default',
        conversation_id: null,
        sessionid: null
    }
});
app.models.chat_state = new app.models.ChatState({id: 1});
app.models.chat_state.fetch();

app.models.Message = Backbone.Model.extend({
    defaults: {
        message: '',
        date: new Date(),
        mine: false
    }
});

app.models.Conversation = Backbone.Collection.extend({
    model: app.models.Message,
    localStorage: new Backbone.LocalStorage('Conversation')
});
app.models.conversation = new app.models.Conversation({id:1});
app.models.conversation.fetch();

app.models.TypingState = Backbone.Model.extend({
    defaults: {
        typing: false,
        last_typing_date: null
    }
});
app.models.mine_typing_state = new app.models.TypingState();
app.models.mine_typing_state.set('last_typing_date', new Date());
app.checkStopTyping = setInterval(function(){
    if (app.models.mine_typing_state.get('typing')){
        var diff = new Date() - window.app.last_typing_date;
        if (diff > 2800){
            app.models.mine_typing_state.set({typing: false});
        }
    }
}, 500);
app.models.mine_typing_state.on('change', function(){
    var event = app.models.mine_typing_state.get('typing') ? 'start_typing' : 'stop_typing';
//    console.log('--- typing state changed ---', event);
    sio.send(
        JSON.stringify({
            event: event,
            conversation_id: app.models.chat_state.get('conversation_id')
        }));
});
app.models.partner_typing_state = new app.models.TypingState();

app.models.AudioState = Backbone.Model.extend({ defaults:{ enabled: true } });
app.models.audio_state = new app.models.AudioState();

app.models.Online = Backbone.Model.extend({ defaults:{ online: 0 } });
app.models.online = new app.models.Online();

/* --- Views --- */
app.views = {};
$(function ($) {
    app.views.MainWindow = Backbone.View.extend({
        el: '#chat-states',
        model: app.models.chat_state,
        states: ['default', 'getpartner', 'conversation', 'reconnect'], // состояния, на которые необходимо реагировать

        initialize: function(){
            this.listenTo(this.model, 'change', this.render);

            $(window).bind('beforeunload',function(e) {
                return 'Если вы закроете страницу, чат прервется!';
            });

        },

        render: function(){
            var current_state = app.models.chat_state.get('state');
            if ($.inArray(current_state, this.states) != -1){ // TODO: заменить
                this.$el.find('.chat-state').hide();
                this.$el.find('.chat-state-'+current_state).show();
            }
        }
    });
    app.views.main_window = new app.views.MainWindow();


    app.views.StartChatBtn = Backbone.View.extend({
        el: '#start-chat',
        model: app.models.chat_state,
        events: {
            click: 'onClick'
        },

        initialize: function(){
            this.listenTo(this.model, 'change', this.render);
        },

        render: function(){
            if (this.model.get('state') == 'ready'){
                this.$el.removeClass('disabled');
            }
        },

        onClick: function(){
            if (this.model.get('state') == 'ready'){
                this.model.set('state', 'getpartner');
            }
        }
    });
    app.views.start_chat_btn = new app.views.StartChatBtn();


    app.views.SocketIO = Backbone.View.extend({
        model: app.models.chat_state,
        initialize: function(){
            this.listenTo(this.model, 'change', this.onChangeState);
        },

        onChangeState: function(){
            var current_state = app.models.chat_state.get('state');
            if (current_state == 'getpartner'){
                sio.send(
                    JSON.stringify({
                        event: 'get_partner'
                    })
                );
            }
        }
    });
    app.views.socket_io = new app.views.SocketIO();


    app.views.StopConversationBtn = Backbone.View.extend({
        el: $('#stop-conversation'),
        model: app.models.chat_state,
        events: {
            click: 'onClick'
        },

        initialize: function(){
            this.listenTo(this.model, 'change', this.render);
        },

        onClick: function(){
            if (confirm('Вы действительно хотите прервать беседу?')) {
                var disconnect_available_states = ['conversation', 'partner_disconnected', 'partner_reconnected'];
                if (jQuery.inArray(this.model.get('state'), disconnect_available_states) != -1){
                    sio.send(
                        JSON.stringify({
                            event: 'terminate_conversation',
                            conversation_id: this.model.get('conversation_id')
                        })
                    );
                }
            }
        },

        render: function(){
            if (this.model.get('state') == 'conversation_terminated'){
                this.$el.hide();
            }
            if (this.model.get('state') == 'conversation'){
                this.$el.show();
            }
        }
    });
    app.views.stop_conversation_btn = new app.views.StopConversationBtn();


    app.views.NewConversationBtn = Backbone.View.extend({
        model: app.models.chat_state,
        template: '<div class="message system">Беседа прервана<\/div><button class="btn">новая беседа<\/button>',
        events: {
            'click .btn': 'onClick'
        },

        render: function(){
            this.$el.html(this.template);
            return this;
        },

        onClick: function(){
            this.model.set('state', 'getpartner');
        }
    });

    app.views.BanrequestBtn = Backbone.View.extend({
        template: '<br><button id="banrequest" class="btn btn-small">не общаться с этим собеседником 3 часа<\/button>',
        events: {
            'click .btn': 'onClick'
        },

        initialize: function(opts){
            this.conversation_id = opts.conversation_id;
        },

        render: function(){
            this.$el.html(this.template);
            return this;
        },

        onClick: function(){
//            console.log('this.conversation_id ', this.conversation_id);
            sio.send(
                JSON.stringify({
                    event: 'banrequest',
                    conversation_id: this.conversation_id
                }));
            this.$el.hide();
        }

    });


    app.views.ConversationForm = Backbone.View.extend({
        el: $('#chat-form'),
        model: app.models.chat_state,
        typing_state_model: app.models.mine_typing_state,

        events: {
            'submit': 'onClick',
            'keypress #chat-form-message': 'onKeyPress'
        },

        initialize: function(){
            this.listenTo(this.model, 'change', this.render);
        },

        render: function(){
            var display_form_states = ['conversation', 'partner_reconnected'];

            if (jQuery.inArray(this.model.get('state'), display_form_states) != -1){
                this.$el.show();
            }else{
                this.$el.hide();
            }

        },

        onClick: function(e){
            e.preventDefault();
            var message = this.$el.find('#chat-form-message').val();
            this.$el.find('#chat-form-message').val('');
            sio.send(
                JSON.stringify({
                    event: 'send_message', conversation_id: this.model.get('conversation_id'), message: message
                })
            );
        },

        onKeyPress: function(e){

            if (e.keyCode == 13) { //(e.ctrlKey || e.metaKey) &&
                this.$el.trigger('submit');
                e.preventDefault();
            }else{
                window.app.last_typing_date = new Date();
                this.typing_state_model.set('typing', true);
            }
        }
    });
    app.views.conversation_form = new app.views.ConversationForm();


    app.views.TypingView = Backbone.View.extend({
        el: $('#typing'),
        model: app.models.partner_typing_state,
        initialize: function(){
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(app.models.chat_state, 'change', this.render);
        },

        render: function(){
//            console.log('-- typing --')
            if (this.model.get('typing') && app.models.chat_state.get('state') == 'conversation'){
                this.$el.text('...✎');
            }else{
                this.$el.text('');
            }
        }
    });
    app.views.typing_view = new app.views.TypingView();


    app.views.Online = Backbone.View.extend({
        el: $('#online'),
        model: app.models.online,
        initialize: function(){
            this.listenTo(this.model, 'change', this.render);
        },

        render: function(){
            this.$el.text('online: '+ this.model.get('online'))
        }
    });
    app.views.online = new app.views.Online();


    app.views.Audio = Backbone.View.extend({
        model: app.models.audio_state,
        el: $('#audio'),
        events:{
            'click': 'change'
        },
        initialize: function(){
            _.bindAll(this);
            this.model.on('change', this.render);

            try{
                this.audio = new Audio('/static/audio/logout.ogg');
            }catch(e){
                this.audio = {
                    play: function(){}
                }
            }
            //this.audio = new Audio('/static/audio/logout.ogg');
//            this.on('play', this.play);
        },
        render: function(){
            if (this.model.get('enabled')){
                this.$el.removeClass('disabled');
            }else{
                this.$el.addClass('disabled');
            }
        },
        play: function(){
            if (this.model.get('enabled')){
//                this.audio.src = '/static/audio/logout.ogg';
//                this.audio.pause();
//                this.audio.currentTime = 0;
                this.audio.play();
            }
        },
        change: function(){
            this.model.set({enabled: !this.model.get('enabled')});
        }
    });
    app.views.audio = new app.views.Audio();


    app.views.ChatLog = Backbone.View.extend({
        el: $('#conversation-log'),
        collection: app.models.conversation,
        model: app.models.chat_state,
        typing_state_model: app.models.mine_typing_state,

        initialize: function(){
            this.listenTo(this.collection, 'add', this.render);
            this.listenTo(this.model, 'change', this.render);

            $('#scroll_down').click(
                function (e) {
                    $('html, body').animate({scrollTop: $('body').height()}, 800);
                }
            );
            $('#scroll_up').click(
                function (e) {
                    $('html, body').animate({scrollTop: '0px'}, 800);
                }
            );
        },

        render: function(msg){
            var self = this;
            var isStateChanged = msg.hasChanged('state');
            if (isStateChanged){
                var state = msg.get('state');

                switch (state){
                    case 'reconnect':
                        this.$el.html('');
                        self.renderMsg('Продолжение беседы');
                        self.renderMsg('--- 10 последних сообщений ---');
                        _.each(self.collection.toArray(), function(el){
                            self.renderMsg(el);
                        });
                        break;

                    case 'conversation':
                        this.$el.html('');
                        self.renderMsg('Поприветствуйте собеседника');
                        break;

                    case 'conversation_terminated':
//                        console.log('--- conversation_terminated ---', );
                        var new_conversation_view = new app.views.NewConversationBtn();
                        var banrequest_view = new app.views.BanrequestBtn({conversation_id: msg.get('conversation_id')});
                        this.renderMsg(new_conversation_view.render().el);
                        this.renderMsg(banrequest_view.render().el);
                        _.each(self.collection.toArray(), function(el){ el.destroy() });
                        break;

                    case 'partner_disconnected':
                        this.renderMsg('Партнер отключился');
                        break;

                    case 'partner_reconnected':
                        this.renderMsg('Партнер вернулся');
                        break;
                }
            }else{
                var message = msg.get('message');
                if (message)
                    this.renderMsg(msg);
            }
            $('#scroll_down').trigger('click');
        },

        renderMsg: function(msg){
            var m = '';
            if (typeof msg == 'string'){
                m = '<div class="message system">'+ msg +'</div>';
            }else if(msg.get != undefined){
                var mine = msg.get('mine') ? 'mine' : '' ;
                m = '<div class="message '+ mine +'">'+ msg.get('message') +'</div>';
                if (mine != 'mine') app.views.audio.play();
            }else{
                m = msg;
            }
            this.$el.append(m);
        }

    });
    app.views.chat_log = new app.views.ChatLog();

    //window.sio = new SockJS('http://'+location.hostname+':8008/chat');  //io.connect('http://'+location.hostname+':8008');
    window.sio = new WebSocket('ws://'+location.hostname+':8008/chat');

    sio.onopen = function(data){
        sio.send(JSON.stringify({event: 'register_user'}));
        app.models.chat_state.save();
        setInterval(function(){ sio.send(JSON.stringify({event: 'online'})) }, 5000);
        sio.send(JSON.stringify({event: 'online'}));
    };


    var message_handlers = {
        user_registered: function(data){
            app.models.chat_state.set('state', 'ready');
        },
        joined_to_conversation: function(data){
            app.models.chat_state.set({state: 'conversation', conversation_id: data.conversation_id});
            app.models.chat_state.save();
        },
        message: function(data){
            if(app.models.conversation.length >= 10){
                var last = app.models.conversation.slice(0,1)[0];
                if (last != undefined){
                    last.destroy();
                }
            }
            app.models.conversation.create({message: data.message, mine: data.mine});
        },
        start_typing: function(){
            app.models.partner_typing_state.set('typing', true);
        },
        stop_typing: function(){
            app.models.partner_typing_state.set('typing', false);
        },

        terminate_conversation: function(data){
            app.models.chat_state.set({state: 'conversation_terminated'});
            app.models.chat_state.set({conversation_id: null});
            app.models.chat_state.save();
        },
        partner_disconnected: function(data){
            app.models.chat_state.set({state: 'partner_reconnected'});
        },
        partner_reconnected: function(data){
            app.models.chat_state.set({state: 'partner_reconnected'});
        },

        online: function(data){
            app.models.online.set('online', data['data']);
        }
    };


    sio.onmessage = function(message){
        var message_data = JSON.parse(message['data']);
        var handler_name = message_data['event'];
        delete message_data['event'];
        message_handlers[handler_name](message_data);
    };

    /*
    $('#reset').click(function(){
        for (var i=0; i<1000; i+=1){
            sio.send(
                JSON.stringify({
                    event: 'send_message', conversation_id: app.models.chat_state.get('conversation_id'), message: i
                })
            );
        }

    });
    $('#reset').tooltip({placement: 'bottom'});
    */
});

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined
}