require 'rubygems'
require 'websocket-client-simple'

ws = WebSocket::Client::Simple.connect 'ws://pogovorisomnoi.ru:8008/chat'
#ws = WebSocket::Client::Simple.connect 'ws://localhost:8080'

ws.on :message do |mm|
  puts mm.data
end


ws.on :close do |e|
  p e
  exit 1
end

ws.on :error do |e|
  p e
end

loop do
  ws.send STDIN.gets.strip
end
